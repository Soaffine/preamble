# Preamble

<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Thanks again! Now go create something AMAZING! :D
***
***
***
*** To avoid retyping too much info. Do a search and replace for the following:
*** AnotherBrynAlt, Ember, AnotherBrynAlt, AnotherBrynAlt@gmail.com, Ember, Ember is a feature-rich Discord Bot written in TypeScript
-->

<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
[![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]

<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://gitlab.com/BrynAlt/preamble">
    <img src="img/tex-symbol.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Preamble</h3>

  <p align="center">
    Preamble is a useful set of packages for LuaLaTeX
    <br />
    <a href="https://gitlab.com/BrynAlt/preamble/-/tree/master/contributing/docs"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/BrynAlt/preamble">View Demo</a>
    ·
    <a href="https://gitlab.com/BrynAlt/preamble/-/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/BrynAlt/preamble/-/issues">Request Feature</a>
  </p>
</p>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->
## About The Project

[![Product Name Screen Shot][product-screenshot]](https://gitlab.com/BrynAlt/preamble)

Preamble is written with modern and highly opinionated style choices.

### Built With

* [___LuaLaTeX___](https://www.luatex.org//)
* [___Visual Studio Code Latex___](https://marketplace.visualstudio.com/items?itemName=James-Yu.latex-workshop)

<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

* For LaTeX texlive

  ```sh
  apt install texlive-full
  ```

* For Code Snippets With Color

  ```sh
  apt install python-pygments
  ```

### Installation

1. Clone the repo

   ```sh
   git clone https://gitlab.com/BrynAlt/preamble.git
   ```

2. Install the Preamble package as a local TeXLive package

   ```sh
   mkdir --parents ~/texmf/tex/latex/local/; cp preamble.sty $_ && texhash ~/texmf
   ```

<!-- USAGE EXAMPLES -->
## Usage

An example PDF and `TeX` file combo is provided to showcase how to use the included packages. Check out the documentation for each package listed in [__**Packages**__](#packages) for more info.

1. At the beginning of a new `.tex` project including the following header. This package runs *best* with `LuaLaTeX` but will also compile with `XeLaTeX` and `pdfLaTeX`.

   ```latex
   %!TeX program = lualatex
   \documentclass{scrartcl}
   \usepackage{preamble}
   ```

2. Alternatively, configure your editor or compiler to prefer LuaLaTeX and then simply.

   ```latex
   \documentclass{scrartcl}
   \usepackage{preamble}
   ```

3. For minted support, add `--shell-escape` to the compile command. If you are running the Latex Workshop extension in VSCode to build, simply add this to your settings.

   ```json
   {"latex-workshop.latex.magic.args": [
    "-synctex=1",
    "-interaction=nonstopmode",
    "-file-line-error",
    "-shell-escape",
    "%DOC%"
   ]}
   ```

<img src="img/shell-escape.png" alt="Shell Escape Settings">

4. Read through [the example PDF](example.pdf) and [example TeX](example.tex) for some common usage considerations.

<!-- ROADMAP -->
## Roadmap

See the [open issues](https://gitlab.com/BrynAlt/preamble/-/issues) for a list of proposed features (and known issues).

<!-- CONTRIBUTING -->
## Contributing

Please read the available documentation in the [Contributing Section](https://gitlab.com/BrynAlt/preamble/-/blob/master/CONTRIBUTING.md)

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<!-- LICENSE -->
## License

 [![License: MIT](https://img.shields.io/badge/License-MIT-red.svg)](https://opensource.org/licenses/MIT)
 [![License: CC0-1.0](https://licensebuttons.net/l/zero/1.0/80x15.png)](http://creativecommons.org/publicdomain/zero/1.0/)

Distributed under the MIT License. See [the MIT license](LICENSE.md) for more information.
The project is also distributed under the CC0 License. See [the CC0 license](license.md) for more information.

Releases of the version of this software that includes the `fitch` package from Peter Selinger may be understood as being distributed under the GPLv2 if, and only if, the interpretation of the extension of the GPLv2 to *Source Code* is upheld.

<!-- CONTACT -->
## Contact

Bryn - [@AnotherBrynAlt](https://twitter.com/AnotherBrynAlt) - AnotherBrynAlt@gmail.com - Bryn#1111 on the Ask Yourself [discord](https://discord.gg/dUPFfby)

Project Link: [Gitlab](https://gitlab.com/BrynAlt/preamble/Ember)

<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements

### Packages

* [microtype](https://ctan.org/pkg/microtype)
* [fontenc](https://ctan.org/pkg/fontenc)
* [array](https://ctan.org/pkg/array)
* [booktabs](https://ctan.org/pkg/booktabs)
* [bookmark](https://ctan.org/pkg/bookmark)
* [xcolor](https://ctan.org/pkg/xcolor)
* [selnolig](https://ctan.org/pkg/selnolig)
* [verbatim](https://ctan.org/pkg/verbatim)
* [amsmath](https://ctan.org/pkg/amsmath)
* [amssymb](https://ctan.org/pkg/amssymb)
* [amsthm](https://ctan.org/pkg/amsthm)
* [nicematrix](https://ctan.org/pkg/nicematrix)
* [physics](https://ctan.org/pkg/physics)
* [siunitx](https://ctan.org/pkg/siunitx)
* [lplfitch](https://ctan.org/pkg/lplfitch)
* [lualatex-math](https://ctan.org/pkg/lualatex-math)
* [unicode-math](https://ctan.org/pkg/unicode-math)
* [datetime2](https://ctan.org/pkg/datetime2)
* [graphicx](https://ctan.org/pkg/graphicx)
* [tikz](https://ctan.org/pkg/tikz)
* [sharp](https://ctan.org/pkg/easylist)
* [listings](https://ctan.org/pkg/listings)
* [upquote](https://ctan.org/pkg/upquote)
* [hyperref](https://ctan.org/pkg/hyperref)
* [minted](https://ctan.org/pkg/minted)
* [etoolbox](https://ctan.org/pkg/etoolbox)
* [fontspec](https://ctan.org/pkg/fontspec)
* [fitch.sty](https://www.mathstat.dal.ca/~selinger/fitch/) \*included in [preamble.sty](preamble.sty)

### Asset Attribution

* [TeX Symbol by Freepik](https://www.flaticon.com/free-icon/tex-symbol_84553)

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/badge/Contributors-gitlab-success.svg
[contributors-url]: https://gitlab.com/BrynAlt/preamble/-/graphs/master
[forks-shield]: https://img.shields.io/badge/Forks-gitlab-informational.svg
[forks-url]: https://gitlab.com/BrynAlt/preamble/-/forks
[stars-shield]: https://img.shields.io/badge/Stars-gitlab-yellow.svg
[stars-url]: https://gitlab.com/BrynAlt/preamble/-/starrers
[issues-shield]: https://img.shields.io/badge/Issues-gitlab-critical.svg
[issues-url]: https://gitlab.com/BrynAlt/preamble/-/issues
[license-shield]: https://img.shields.io/badge/License-MIT-red.svg
[license-url]: https://gitlab.com/BrynAlt/preamble/-/blob/master/contributing/license/mit.md
[product-screenshot]: img/latex.png
